# Instalação

- [Instalação](#installation)
    - [Requisitos Básicos](#server-requirements)
    - [Instalando](#installing)
    - [Configurando](#configuration)

<a name="installation"></a>
## Instalação

<a name="server-requirements"></a>
### Requisitos Básicos

O sistema Steel4Web requer algumas configurações expecíficas para seu funcionamento adequado. Certifique-se que o seu servidor possui os seguintes requisitos:

<div class="content-list" markdown="1">
- PHP >= 5.6.4
- Extensão PHP OpenSSL
- Extensão PHP PDO
- Extensão PHP Mbstring
- Extensão PHP Tokenizer
- Extensão PHP XML
</div>

<a name="installing"></a>
### Instalando

Clone o repositório da aplicação ou baixe manualmente.

Entre na pasta e execute o comando:

    composer global require "laravel/installer"

<a name="configuration"></a>
### Configurando

... continuar aqui
